/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure;

import java.util.ArrayList;

public class Player {
    
    private String name;
    private String pirateOrNinja;
    public ArrayList<Item> inventory = new ArrayList();
    public Location currentLoc;
    
    public Player(String name, String pirateOrNinja, Location currentLoc){
        
        this.name = name;
        this.pirateOrNinja = pirateOrNinja;
        this.currentLoc = currentLoc;
        
    }
    
    
    public void addToInventory(Item item){
        this.inventory.add(item);
    }
    
    
    public void getItems(){
        GUI.items.setText("");
        for(int i = 0; i < inventory.size(); i++){
            GUI.items.setText(GUI.items.getText() + "<html><br>" +inventory.get(i).getName());
            //output += inventory.get(i).getName() + "\n";
        }
        GUI.items.setText("<html><center>ITEMS</center> <br>" + GUI.items.getText());
        
    }
    
    public void drop(String item){
        
        for(int i = 0; i < inventory.size(); i++){
            if(inventory.get(i).getName().equals(item)){
                this.inventory.remove(i);
            }
        }
    }
    /**
     * Checks if the token the user entered is a valid location. Then checks 
     * if that location is adjacent to the current location. If both are true 
     * the current location is changed.
     * @param newLoc
     * @return 
     */
    public boolean move(String newLoc){
        boolean exists = false;
        for(int i = 0; i < World.myWorld.locArr.size(); i++){
            if(newLoc.equals( World.myWorld.locArr.get(i).getName()) ){
                if(World.myWorld.gameWorld[World.myWorld.getIndex(currentLoc)][i] == 1){
                    this.currentLoc = World.locArr.get(i);
                    World.trail.add(0, currentLoc);
                    return true;
                }
                GameEngine.display("You can't get there from here!");
                exists = true;
            }
        }
        if(exists == false){
            GameEngine.display("That location doesn't exist.");
        }
        return false;
    }
    
    public void take(String item){
        item = item.toLowerCase();
        GameEngine.display(item);
        if(this.currentLoc.has(item)){
            for(int i = 0; i < currentLoc.items.size(); i++){
                if(currentLoc.getItems().get(i).getName().equals(item)){
                    this.addToInventory(currentLoc.getItems().get(i));
                    GameEngine.display(item + " was successfully added.");
                }
            }
        }
        else{
            GameEngine.display("There's no item like that here!");
        }
    }
    
    public void backtrack(String steps){
        
        int num = 0;
        
        try{
            num = Integer.parseInt(steps);
        } catch(NumberFormatException e) {
            
            GameEngine.display("Please enter a number when backtracking.");
        }
        
        if(World.trail.size() > num){
            this.currentLoc = World.trail.get(num);

            for(int i = num; num > 0; num--){
                World.trail.remove(World.trail.size()-i);
            }
        }
        else{
            GameEngine.display("You can't go back that far!");
        }
            
        
    }

}

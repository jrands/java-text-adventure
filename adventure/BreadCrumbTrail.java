/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure;

/**
 *
 * @author John
 */
public class BreadCrumbTrail {
        
    private class BreadCrumb{
        
        private Location value;
        private BreadCrumb next;
        
        public BreadCrumb(Location value, BreadCrumb next){
            
            this.next = next;
            this.value = value;
            
        }
        
        public BreadCrumb(Location value){
            this(value, null);
        }
    }
    
    private BreadCrumb first;
    private BreadCrumb last;
    
    public BreadCrumbTrail(){
        
        first = null;
        last= null;
        
    }
    
    public boolean isEmpty(){  
        
        return first == null; 
        
    }
    
    public int size(){
        
       int count = 0;
       BreadCrumb p = first;     
		 while (p != null)
       {
           // There is an element at p
           count ++;
           p = p.next;
       }
       return count;
       
    }
    
    public void add(Location e)
    {
      if (isEmpty()) 
      {
          first = new BreadCrumb(e);
          last = first;
      }
      else
      {
          // Add to end of existing list
          last.next = new BreadCrumb(e);
          last = last.next;
      }      
    }

    public Location get(int index)
    {
         if (index < 0  || index > size()) 
         {
             String message = String.valueOf(index);
             throw new IndexOutOfBoundsException(message);
         }
         
         // search sequentially for the node at the desired position
         BreadCrumb crumb = first;        
         for (int k = 0; k < index; k++)        
         {
            crumb = crumb.next;           
         }
         
         return crumb.value;
    }
    
    public void add(int index, Location e)
    {
         if (index < 0  || index > size()) 
         {
             String message = String.valueOf(index);
             throw new IndexOutOfBoundsException(message);
         }
         
         // Index is at least 0
         if (index == 0)
         {
             // New element goes at beginning
             first = new BreadCrumb(e, first);
             if (last == null)
                 last = first;
             return;
         }
         
         // Set a reference pred to point to the node that
         // will be the predecessor of the new node
         BreadCrumb pred = first;        
         for (int k = 1; k <= index - 1; k++)        
         {
            pred = pred.next;           
         }
         
         // Splice in a node containing the new element
         pred.next = new BreadCrumb(e, pred.next);  
         
         // Is there a new last element ?
         if (pred.next.next == null)
             last = pred.next;         
    }
    
    /**
       The toString method computes the string
       representation of the list.
       @return The string form of the list.
    */
    public String toString()
    {
      StringBuilder strBuilder = new StringBuilder();
      
      // Use p to walk down the linked list
      BreadCrumb p = first;
      while (p != null)
      {
         strBuilder.append(p.value + "\n"); 
         p = p.next;
      }      
      return strBuilder.toString(); 
    }
    
      /**
       The remove method removes the element at an index.
       @param index The index of the element to remove. 
       @return The element removed.  
       @exception IndexOutOfBoundsException When index is 
                  out of bounds.     
    */
    public Location remove(int index)
    {
       if (index < 0 || index >= size())
       {  
           String message = String.valueOf(index);
           throw new IndexOutOfBoundsException(message);
       }
       
       Location element;  // The element to return     
       if (index == 0)
       {
          // Removal of first item in the list
          element = first.value;    
          first = first.next;
          if (first == null)
              last = null;             
       }
       else
       {
          // To remove an element other than the first,
          // find the predecessor of the element to
          // be removed.
          BreadCrumb pred = first;
          
          // Move pred forward index - 1 times
          for (int k = 1; k <= index -1; k++)
              pred = pred.next;
          
          // Store the value to return
          element = pred.next.value;
          
          // Route link around the node to be removed
          pred.next = pred.next.next;  
          
          // Check if pred is now last
          if (pred.next == null)
              last = pred;              
       }
       return element;        
    }
    
    /**
       The remove method removes an element.
       @param element The element to remove.
       @return true if the remove succeeded, 
		 false otherwise.
    */
    
    public boolean remove(Location element)
    {
       if (isEmpty()) 
           return false;      
      
       if (element.equals(first.value))
       {
          // Removal of first item in the list
          first = first.next;
          if (first == null)
              last = null;       
          return true;
       }
      
      // Find the predecessor of the element to remove
      BreadCrumb pred = first;
      while (pred.next != null && 
             !pred.next.value.equals(element))
      {
          pred = pred.next;
      }

      // pred.next == null OR pred.next.value is element
      if (pred.next == null)
          return false;
      
      // pred.next.value  is element
      pred.next = pred.next.next;    
      
      // Check if pred is now last
      if (pred.next == null)
          last = pred;
      
      return true;       
    }


    /**
     * Checks whether a string appears in this list.
     */
    public boolean contains ( Location str )
    {
        boolean result = false;
        
        BreadCrumb ref = this.first;
        
        while ( ref != null ) {
        
            if ( ref.value.equals( str ) ) {
                result = true;
                break;
            }

            ref = ref.next;
        }
        
        return result;
    }
    
}
